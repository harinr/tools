﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLiteCodeGen
{
    public partial class frmCodeGenerator : Form
    {
        SQliteSchema lib;
        private string templatePath = @"templates\SQLite\";

        public frmCodeGenerator()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            

            lib = new SQliteSchema(textBox1.Text);

            List<string> t = lib.Tables;

            foreach(string s in t)
            {
                comboBox1.Items.Add(s);
            }

            if (comboBox1.Items.Count > 1)
                comboBox1.SelectedIndex = 0;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<SQLiteColumn> cols =  lib.Columns(comboBox1.Text);
            dataGridView1.DataSource = cols;

            textBox3.Text =  GetInsertCommand(cols, comboBox1.Text);

        }

        private string GetInsertCommand(List<SQLiteColumn> cols, string tableName)
        {
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbParameters = new StringBuilder();
            StringBuilder sbPK = new StringBuilder();
            StringBuilder sbPKCheck = new StringBuilder();
            StringBuilder sbUpdate = new StringBuilder();

            string format = "";

            foreach (SQLiteColumn col in cols)
            {
                sbColumns.Append(col.Name + ", ");

                if(col.Name.Trim().ToLower().Equals("lastupdateddate") || col.Name.Trim().ToLower().Equals("updatedon") || col.Name.Trim().ToLower().Equals("lastupdatedate"))
                { 
                    sbValues.Append("datetime('now', 'localtime'), ");
                    sbUpdate.Append(col.Name + "= datetime('now', 'localtime'), ");
                }
                else
                { 
                    sbValues.Append("@" + col.Name + ", ");

                    if(!col.PrimaryKey)
                        sbUpdate.Append(col.Name + "= @" + col.Name + ", ");

                    sbParameters.Append("\t\t\t\tcmd.Parameters.AddWithValue(" + "\"");
                    sbParameters.Append("@" + col.Name + "\"" + ",dr[" + "\"");
                    sbParameters.AppendLine(col.Name + "\"" + "].ToString());");

                    if (col.Type.ToLower().StartsWith("date"))
                    {
                        if (col.Type.ToLower().EndsWith("time"))
                            format = "yyyy-MM-dd hh:mm:ss";
                        else
                            format = "yyyy-MM-dd";
                        
                        sbParameters.AppendLine("");
                        sbParameters.AppendLine("\t\t\t\tif(dr[" + "\"" + col.Name + "\"" + "].ToString().Length != 0)");
                        sbParameters.Append("\t\t\t\t\tcmd.Parameters.AddWithValue(" + "\"");
                        sbParameters.Append("@" + col.Name + "\"" + ", DateTime.Parse(dr[" + "\"");
                        sbParameters.AppendLine(col.Name + "\"" + "].ToString()).ToString(\"" + format + "\"" + "));");
                        sbParameters.AppendLine("");
                    }
                             
                }

                if (col.PrimaryKey)
                {
                    sbPK.Append("\t" + col.Name + "= @" + col.Name + " and ");
                    string s = "dt.Rows[0][" + "\"" + col.Name + "\"" + "].ToString().Trim().Length > 0 && ";

                    sbPKCheck.Append(s);
                }
            }

            string columns = sbColumns.ToString().Trim();
            columns = columns.Substring(0, columns.Length - 1) + ")";

            string values = sbValues.ToString().Trim();
            values = values.Substring(0, values.Length - 1) + ")";

            string parameters = sbParameters.ToString().Trim();

            string pk = sbPK.ToString().Trim();
            if(pk.Length > 10)
                pk = pk.Substring(0, pk.Length - 3);

            string pkCheck = sbPKCheck.ToString().Trim();
            if (pkCheck.Length > 10)
                pkCheck = pkCheck.Substring(0, pkCheck.Length - 3);

            string udpateCol = sbUpdate.ToString().Trim();
            udpateCol = udpateCol.Substring(0, udpateCol.Length-1);

            string code = System.IO.File.ReadAllText(templatePath + "Template.txt");

            code = code.Replace("{TableName}", tableName);
            code = code.Replace("{Columns}", columns);
            code = code.Replace("{Values}", values);
            code = code.Replace("{Parameters}", parameters);
            //{PKCheck}, {PrimaryKeys}

            if (pkCheck.Length == 0)
                pkCheck = "false";

            code = code.Replace("{PKCheck}", pkCheck);
            code = code.Replace("{PrimaryKeys}", pk);
            code = code.Replace("{UpdateColumns}", udpateCol);

            if (pk.Length == 0)
                code = code.Replace("{CommentUpdate}", "//");
            else
                code = code.Replace("{CommentUpdate}", "");

            return code;


        }

        //private void radioButton2_CheckedChanged(object sender, EventArgs e)
        //{
        //    templatePath = @"templates\SQLite\";
        //}

        //private void radioButton1_CheckedChanged(object sender, EventArgs e)
        //{
        //    templatePath =  @"templates\VistaDB\";
        //}

        private void OnDBTypeClick(object sender, EventArgs e)
        {
            RadioButton c = (RadioButton)sender;

            templatePath = string.Format(@"templates\{0}\", c.Text.Trim());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
