﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace SQLiteCodeGen
{
    public class SQliteSchema
    {
        public SQLiteConnection DbConnection { get; set; }
        public List<string> Tables { get; set; }
        public List<string> Views { get; set; }


        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="dbname"></param>
        public SQliteSchema(string dbname)
        {
            SQLiteConnection c = new SQLiteConnection();
            c.ConnectionString = @"data source=" + dbname;
            c.Open();

            Tables = new List<string>();
            Views = new List<string>();

            this.DbConnection = c;
            this.RefreshSchema();
        }

        public void RefreshSchema()
        {
            this.Tables.Clear();
            this.Views.Clear();
            using (SQLiteCommand cmd = this.DbConnection.CreateCommand())
            {
                cmd.CommandText = "select name from sqlite_master where type='table' order by name";
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    this.Tables.Add(dr["name"].ToString());
                }
                dr.Close();
                cmd.CommandText = "select name from sqlite_master where type='view' order by name";
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    this.Views.Add(dr["name"].ToString());
                }
                dr.Close();
            }
        }

        public List<SQLiteColumn> Columns(string tableName)
        {
            List<SQLiteColumn> lst = new List<SQLiteColumn>();
            using (SQLiteCommand cmd = this.DbConnection.CreateCommand())
            {
                cmd.CommandText = string.Format("pragma table_info({0})", tableName);
                SQLiteDataReader dr = cmd.ExecuteReader();
                bool notNull = false;
                bool isPk = false;
                while (dr.Read())
                {
                    if (dr["notnull"].ToString().Equals("1"))
                        notNull = true;
                    else
                        notNull = false;

                    if (!dr["pk"].ToString().Equals("0"))
                        isPk = true;
                    else
                        isPk = false;

                    lst.Add(new SQLiteColumn(tableName, dr["type"].ToString(), Convert.ToInt32(dr["cid"].ToString()), dr["name"].ToString(),
                        notNull, isPk));
                }
                dr.Close();
            }
            return lst;
        }
    }

    public class SQLiteColumn
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="table"></param>
        /// <param name="type"></param>
        /// <param name="cId"></param>
        /// <param name="name"></param>
        /// <param name="notNull"></param>
        /// <param name="primaryKey"></param>
        /// <remarks></remarks>
        public SQLiteColumn(string table, string type, int cId, string name, bool notNull, bool primaryKey)
        {
            this.Table = table;
            this.Type = type;
            this.CId = cId;
            this.Name = name;
            this.NotNull = notNull;
            this.PrimaryKey = primaryKey;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <remarks></remarks>
        public SQLiteColumn()
        {
        }

        /// <summary>
        /// The table that owns this column.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string Table { get; set; }

        /// <summary>
        /// The data type of the column.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string Type { get; set; }

        /// <summary>
        /// The column ID which represents the ordinal the column is in order in the table.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public int CId { get; set; }

        /// <summary>
        /// The name of the column.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string Name { get; set; }

        /// <summary>
        /// Whether a column can contain null data or not.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool NotNull { get; set; }

        /// <summary>
        /// Whether the column is a primary key or not.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool PrimaryKey { get; set; }
    }
}
